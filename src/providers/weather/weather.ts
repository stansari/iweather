import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
@Injectable()
export class WeatherProvider {
  url;
  constructor(public http: Http) {
    console.log('Hello WeatherProvider Provider');
    this.url = 'https://www.accuweather.com/en/in/india-weather';
  }

  getWeather(city, state){
    return this.http.get(this.url+'/'+state+'/'+city)
    .map(res => res.json());
  }

}
